public class BaseException extends Exception {
    public BaseException(String error){
        super(error);
    }
    public BaseException(){
        super();
    }
    public BaseException(Exception exception){
        super(exception);
    }
    public BaseException(String error, Exception exception){
        super(error, exception);
    }
}
