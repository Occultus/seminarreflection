import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class Service {

    public static @NotNull List<String> getInfo(List<Base> objects) throws ServiceException {
        if(objects == null){
            throw new ServiceException("Objects is Null pointer");
        }
        List<String> res = new ArrayList<>();
        for(Base temp: objects){
            if(temp instanceof Derived){
                res.add(temp.getDescription() + ", " + ((Derived) temp).getAdditionalDescription());
            }else {
                res.add(temp.getDescription());
            }

        }
        return res;
    }

    public static boolean isFunctionalInterface(Class<?> cls) throws ServiceException{
        if(cls == null){
            throw new ServiceException("class is Null pointer");
        }
        if(!cls.isInterface() && cls.getMethods().length != 1){
            return false;
        }
        int mod = cls.getMethods()[0].getModifiers();
        return Modifier.isPublic(mod) && Modifier.isAbstract(mod);
    }

    public static boolean isSerializable(Object obj) throws ServiceException {
        if(obj == null){
            throw new ServiceException("object is Null pointer");
        }

        Class<?>[] ifs = obj.getClass().getInterfaces();
        for(Class<?> ifc : ifs){
            if(ifc.getName().equals("java.io.Serializable")){
                return true;
            }
        }

        return false;
    }

    private static boolean checkInterfaceImplementsSerializable(Object object){
        if(object == null){
            return false;
        }
        Class<?>[] ifs = object.getClass().getInterfaces();
        if(ifs == null){
            return false;
        }
        for(Class<?> ifc : ifs){
            if(ifc.getCanonicalName().equals("java.io.Serializable") ||
                    checkInterfaceImplementsSerializable(ifc)){
                return true;
            }
        }

        return false;
    }


    private static boolean checkObjectExtendSerializable(Class<?> cls){
        if(cls == null){
            return false;
        }
        Class<?>[] ifs = cls.getInterfaces();

        for(Class<?> ifc : ifs){
            if(ifc.getCanonicalName().equals("java.io.Serializable")){
                return true;
            }
        }

        return checkObjectExtendSerializable(cls.getSuperclass());

    }

    public static boolean isHiddenSerializable(Object object) throws ServiceException {
        if(object == null){
            throw new ServiceException("object is Null pointer");
        }

        return checkObjectExtendSerializable(object.getClass())
                || checkInterfaceImplementsSerializable(object);
    }

    public static @NotNull
    List<String> classesWithStaticMethod(List<Class<?>> objects) throws ServiceException {
        if(objects == null){
            throw new ServiceException("objects is Null pointer");
        }
        List<String> res = new ArrayList<>();
        for(Class<?> temp: objects){
            Method[] tempMethods = temp.getMethods();
            boolean hasStatic = false;
            for(int i = 0; i < tempMethods.length && !hasStatic; i++){
                if(Modifier.isStatic(tempMethods[i].getModifiers())){
                    hasStatic = true;
                    res.add(temp.getCanonicalName());
                }
            }
        }
        return res;
    }

}
