class Keys{
    private String variety;
    Keys(String variety){
        this.variety = variety;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public int getCountOfKeys(){
        return 5;
    }

}