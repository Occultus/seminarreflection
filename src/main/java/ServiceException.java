public class ServiceException extends Exception {
    public ServiceException(String error){
        super(error);
    }
    public ServiceException(){
        super();
    }
    public ServiceException(Exception exception){
        super(exception);
    }
    public ServiceException(String error, Exception exception){
        super(error, exception);
    }
}
