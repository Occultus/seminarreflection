import java.util.Objects;

public class Base {
    String description;

    public Base(String description) throws BaseException {
        if(description == null || description.isEmpty()){
            throw new BaseException("String is Null pointer/empty");
        }
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Base base = (Base) o;
        return Objects.equals(description, base.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }
}
