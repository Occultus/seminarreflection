import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;

public class ServiceTests {
    @Test
    public void getInfoTest() throws ServiceException, BaseException {
        assertThrows(ServiceException.class, ()->Service.getInfo(null));
        Base base = new Base("glass");
        Base base1 = new Base("iron");
        Derived base2 = new Derived("Material", "silk");

        List<String> res = Service.getInfo(Arrays.asList(base, base1, base2));
        List<String> expRes = Arrays.asList("glass", "iron", "Material, silk");
        assertEquals(expRes, res);

    }

    @FunctionalInterface
    interface MyPredicate {
        boolean test(Integer value);
    }

    @Test
    public void isFunctionalInterfaceTest() throws ServiceException {
        class Animal{
            private final String continent = "Africa";

            public int getLegs(){
                return 4;
            }
        }

        assertTrue(Service.isFunctionalInterface(MyPredicate.class));
        assertFalse(Service.isFunctionalInterface(Animal.class));
        assertTrue(Service.isFunctionalInterface(Predicate.class));

    }

    @Test
    public void isSerializableTest() throws ServiceException {
        class Cat implements Serializable{
            private final String name = "Barsik";
        }

        class Horse {
            private final int tail = 1;
        }

        assertTrue(Service.isSerializable(new Cat()));
        assertFalse(Service.isSerializable(new Object()));
        assertFalse(Service.isSerializable(new Horse()));

    }

    interface dataSaving extends Serializable{
        public int savingData();
    }

    interface superDataSaving extends dataSaving{
        public int superSavingData();
    }

    @Test
    public void issSerializableTest() throws ServiceException {

        class Horse implements superDataSaving {
            private final int tail = 1;

            @Override
            public int superSavingData() {
                return 1001110;
            }

            @Override
            public int savingData() {
                return 1100;
            }
        }

        class Dog implements Serializable{
            public final int legs = 4;
        }

        class Afador extends Dog{
            public final String size = "20-24 inches";
        }

        class BlackAfador extends Afador{
            public final String color = "Black";
        }

        assertTrue(Service.isHiddenSerializable(new Horse()));
        assertTrue(Service.isHiddenSerializable(new BlackAfador()));
    }

    @Test
    public void classesWithStaticMethodTest() throws ServiceException {
        assertThrows(ServiceException.class, ()->Service.classesWithStaticMethod(null));

        List<String> expRes = Arrays.asList("Robot", "Npc", "java.util.Arrays", "java.lang.System");
        List<Class<?>> clsList = Arrays.asList(Robot.class, Keys.class, Npc.class, Object.class, Arrays.class,
                System.class);

        assertEquals(expRes, Service.classesWithStaticMethod(clsList));

    }

}
